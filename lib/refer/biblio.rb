class Biblio
    attr_accessor :autores, :titulo, :serie, :editorial, :edicion, :fecha, :isbn
    
    def initialize(autores, titulo, serie = false, editorial, edicion, fecha, isbn)
        @autores = autores
        @titulo = titulo
        @serie = serie;
        @editorial = editorial
        @edicion = edicion
        @fecha = fecha
        @isbn = isbn
    end

    def to_s
        puts "#{autores}"
        puts "#{titulo}"
        puts "#{serie}" if serie
        puts "#{editorial}"
        puts "#{edicion}"
        puts "#{fecha}"
        puts "#{isbn}"
    end
end